package com.desh.springboot.service;

import java.util.List;

import com.desh.springboot.model.Expense;

public interface ExpenseService {

	Expense save(Expense expense);

	Expense update(Expense expense);

	void delete(Expense expense);

	Expense getById(Long Id);

	List<Expense> getAll();
}
