package com.desh.springboot.service;

import java.util.List;

import com.desh.springboot.model.Category;

public interface CategoryService {

	Category save(Category category);

	Category update(Category category);

	void delete(Category category);

	Category getById(Long Id);

	List<Category> getAll();
}
