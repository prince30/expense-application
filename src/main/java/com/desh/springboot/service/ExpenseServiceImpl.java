package com.desh.springboot.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.desh.springboot.model.Expense;
import com.desh.springboot.repository.ExpenseRepository;

@Service
public class ExpenseServiceImpl implements ExpenseService {
	
	@Autowired
	private ExpenseRepository expenseRepository;

	@Override
	public Expense save(Expense expense) {
		return expenseRepository.save(expense);
	}

	@Override
	public Expense update(Expense expense) {
		return expenseRepository.save(expense);
	}

	@Override
	public void delete(Expense expense) {
		expenseRepository.delete(expense);
	}

	@Override
	public Expense getById(Long Id) {
		return expenseRepository.findById(Id).get();
	}

	@Override
	public List<Expense> getAll() {
		return expenseRepository.findAll();
	}

}
