package com.desh.springboot.model;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "expenses")
public class Expense extends AbstractEntity {

	private String description;
	
	@Temporal(TemporalType.DATE)
	private Date expenseDate;
	private String location;

	@ManyToOne
	private Category category;

	@JsonIgnore
	@ManyToOne
	private User user;

	public Expense() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Expense(String description, Date expenseDate, String location, Category category, User user) {
		super();
		this.description = description;
		this.expenseDate = expenseDate;
		this.location = location;
		this.category = category;
		this.user = user;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getExpenseDate() {
		return expenseDate;
	}

	public void setExpenseDate(Date expenseDate) {
		this.expenseDate = expenseDate;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}
