package com.desh.springboot.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.desh.springboot.model.Expense;

public interface ExpenseRepository extends JpaRepository<Expense, Long> {

}
