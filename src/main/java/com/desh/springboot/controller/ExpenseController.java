package com.desh.springboot.controller;

import java.util.List;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.desh.springboot.model.Expense;
import com.desh.springboot.service.ExpenseService;

@RestController
@RequestMapping("/expenses")
public class ExpenseController {

	private ExpenseService expenseService;

	public ExpenseController(ExpenseService expenseService) {
		super();
		this.expenseService = expenseService;
	}
	
	@GetMapping
	public List<Expense> getAll() {
		return expenseService.getAll();
	}
	
	@GetMapping("/{id}")
	public Expense show(@PathVariable("id") Long id) {
		return expenseService.getById(id);
	}

	@PostMapping
	public Expense store(@RequestBody Expense expense) {
		return expenseService.save(expense);
	}

	@PutMapping
	public Expense update(@RequestBody Expense expense) {
		return expenseService.update(expense);
	}

	@DeleteMapping("/{id}")
	public void delete(@PathVariable("id") 	Long id) {
		Expense expense = expenseService.getById(id);
		expenseService.delete(expense);
	}
}
