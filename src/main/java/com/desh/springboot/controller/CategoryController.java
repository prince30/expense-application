package com.desh.springboot.controller;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.desh.springboot.model.Category;
import com.desh.springboot.service.CategoryService;

@RestController
@RequestMapping("/categories")
public class CategoryController {

	private CategoryService categoryService;

	public CategoryController(CategoryService categoryService) {
		super();
		this.categoryService = categoryService;
	}
	
	@GetMapping
	public List<Category> getAll() {
		return categoryService.getAll();
	}
	
	@GetMapping("/{id}")
	public Category show(@PathVariable("id") Long id) {
		return categoryService.getById(id);
	}

	@PostMapping
	public Category store(@ModelAttribute("category") Category category) {
		return categoryService.save(category);
	}

	@PutMapping
	public Category update(@ModelAttribute("category") Category category) {
		return categoryService.update(category);
	}

	@DeleteMapping("/{id}")
	public void delete(@PathVariable("id") 	Long id) {
		Category category = categoryService.getById(id);
		categoryService.delete(category);
	}
}
