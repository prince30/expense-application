import React from 'react';
import logo from './logo.svg';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import AppNavbar from  './components/AppNavbar';
import MyRoute from  './components/MyRoute';

function App() {
  return ( 
    <div className = "App">
		<AppNavbar/>
		<MyRoute/>
    </div>
  );
}

export default App;