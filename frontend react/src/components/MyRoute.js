import React, { Component } from 'react';
import {Route, BrowserRouter as Router, Switch} from 'react-router-dom';
import Category from  './Category';
import Expense from  './Expense';
import NotFound from  './NotFound';

class MyRoute extends Component {
    render() {
        return (
            <div>
				<Router>
					<Switch>
					<Route exact path="/Category" component={Category} />
					<Route exact path="/expense" component={Expense} />
					</Switch>
				</Router>
            </div>
        );
    }
}

export default MyRoute;