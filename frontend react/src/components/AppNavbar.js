import React, { Component } from 'react';
import {Navbar, NavbarBrand, Nav, NavItem, NavLink } from 'reactstrap';

class AppNavbar extends Component {
	
    render() {
        return (
			<div>
			  <Navbar color="dark" dark expand="md">
				<NavbarBrand href="/">Expense Tracker Application</NavbarBrand>
				<Nav className="mr-auto" navbar>
					<NavItem>
					  <NavLink href="/">Home</NavLink>
					</NavItem>
					<NavItem>
					  <NavLink href="/category">Category</NavLink>
					</NavItem>
					<NavItem>
					  <NavLink href="/expense">Expense</NavLink>
					</NavItem>
				</Nav>	
			  </Navbar>
			</div>
		  );
    }
}

export default AppNavbar;