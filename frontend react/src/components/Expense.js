import React, { Component } from 'react';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { select, Container, Col, Button, Form, FormGroup, Label, Input, Table } from 'reactstrap';
import axios from 'axios';


class Expense extends Component {
	
	constructor()
	{
		super()
		this.state={
			expenseDate:new Date(),
			expenses:[],
			categories:[],
			description:"",
			category:{id:'', name:''},
			location:""
		}
		
		this.handleSubmit=this.handleSubmit.bind(this);
		this.handleChange=this.handleChange.bind(this);
		this.handleSelectChange=this.handleSelectChange.bind(this);
		this.handleDateChange=this.handleDateChange.bind(this);
		this.handleDelete=this.handleDelete.bind(this);
	}
	
	componentDidMount() 
	{	
		axios.get(`/categories`)
		.then(response => {
			console.log(response.data);
			this.setState({categories:response.data})
		})
		.catch(error=> {
		  console.log(error);
		})
		
		axios.get(`/expenses`)
		.then(response => {
			console.log(response.data);
			this.setState({expenses:response.data})
		})
		.catch(error=> {
		  console.log(error);
		})
	}
	
	handleChange=(event)=>{		
		this.setState({[event.target.name] : event.target.value})
	}
	
	handleSelectChange=(event)=>{		
		this.setState({ category: {id:event.target.value} })
	}
	
	handleDateChange=(date)=>{		
		this.setState({expenseDate: date})
	}
	
	handleDelete=(id)=>{	
		axios.delete(`/expenses/${id}`)
		.then(response => {
		})
	}
	
	handleSubmit=()=>{
		
		var postData={
			description:this.state.description,
			category:this.state.category,
			location:this.state.location,
			expenseDate:this.state.expenseDate
		}

		axios.post(`/expenses`, postData)
		.then(response => {
			console.log(response.data);
		})
		.catch(error=> {
		  console.log(error);
		})
	}
	
    render() {
		const title=<h1>Add Expense</h1>
		const data = this.state.categories;
		let name =data.map((category) => {
			return <option value={category.id} key={category.id}>{category.name}</option>
		})
		
		const expenses = this.state.expenses;
		let rows =expenses.map((expense, index) => {
			 	return	(<tr>
						  <td>{++index}</td>
						  <td>{expense.description}</td>
						  <td>{expense.location}</td>
						  <td>{expense.expenseDate}</td>
						  <td>{expense.category.name}</td>
						  <td><Button className="btn btn-danger btn-sm" onClick={()=>this.handleDelete(expense.id)}>Delete</Button></td>
						</tr>)
		})
		
        return (
            <div>
                <Container>
					{title}
					<Form>
					  <FormGroup row>
						<Label for="exampleEmail">Title</Label>
						<Col sm={10}>
						  <Input type="text" name="description" id="description" onChange={this.handleChange} placeholder="" />
						</Col>
					  </FormGroup>
					  <FormGroup row>
						<Label for="exampleSelect" sm={2}>Category</Label>
						<Col sm={10}>
						  <select  onChange={this.handleSelectChange}>
						  <option>Select Category</option>
							{name}
						  </select>
						</Col>
					  </FormGroup>
					   <FormGroup row>
						<Label for="exampleSelect" sm={2}>Date</Label>
						<Col sm={10}>
						  <DatePicker 
						  name="expenseDate" 
						  selected={this.state.expenseDate} 
						  onChange={this.handleDateChange}
						  dateFormat="MM/dd/yyyy"
						  />
						</Col>
					  </FormGroup>
					  <FormGroup row>
						<Label for="exampleEmail">Location</Label>
						<Col sm={10}>
						  <Input type="text" name="location" id="location" onChange={this.handleChange} />
						</Col>
					  </FormGroup>
					  <FormGroup check row>
						<Col sm={{ size: 10, offset: 2 }}>
						  <Button onClick={this.handleSubmit}>Submit</Button>
						</Col>
					  </FormGroup>
					</Form>
				</Container>
				<hr/>
				<Container>
					<Table bordered>
					  <thead>
						<tr>
						  <th>S/N</th>
						  <th>Title</th>
						  <th>Location</th>
						  <th>Date</th>
						  <th>Category</th>
						  <th>Action</th>
						</tr>
					  </thead>
					  <tbody>
							{rows}
					  </tbody>
					</Table>
				</Container>
            </div>
        );
    }
}

export default Expense;