import React, { Component } from 'react';
import axios from 'axios';

class Category extends Component {
	
	constructor()
	{
		super()
		this.state={
			categories:[]
		}
	}
	
	componentDidMount() 
	{	
	  axios.get(`/categories`)
		.then(response => {
			console.log(response.data);
			this.setState({categories:response.data})
		})
		.catch(error=> {
		  console.log(error);
		})
	}
	
    render() {
		const data = this.state.categories;
		const name =data.map((data) => {
			return <li>{data.name}</li>
		})
		
        return (
            <div>
                <ul>{name}</ul>
            </div>
        );
    }
}

export default Category;